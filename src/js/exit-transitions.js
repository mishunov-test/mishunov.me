import(/* webpackChunkName: 'exit-transitions-css' */'../css/exit-transitions.css');
import { TimelineLite, Power2, Power0, CSSPlugin} from './gsap-modules';
import { BezierPlugin, Elastic } from 'gsap/all';
import MorphSVGPlugin from './gsap-bonus/MorphSVGPlugin.js';

const plugins = [MorphSVGPlugin, CSSPlugin, BezierPlugin];

function _bootstrapTransition(sectionOut, type='eraser') {
  let transitionStyle = document.querySelector(`.transition-el.${type}`);
  if (!transitionStyle) {
    const template = document.getElementById(`${type}-transition`);
    const clone = document.importNode(template.content, true);
    document.body.appendChild(clone);
    transitionStyle = document.querySelector(`.transition-el.${type}`);
  }
  const char = document.getElementById(sectionOut).querySelector('.body-contour > g');
  const charRect = char.getBoundingClientRect();
  const tl = new TimelineLite({});

  tl.set(transitionStyle, {
    autoAlpha: 1,
    'margin-left': `${charRect.left}px`,
    'margin-top': `${charRect.top}px`,
    'width': `${charRect.width}px`,
    'height': `${charRect.height}px`
  });

  return [tl, transitionStyle, charRect];
}

function _handEraserAnimation(sectionOut, transitionStyle, charRect) {
  const hand = transitionStyle.querySelector('img');
  const path = transitionStyle.querySelector('path');
  const resPathRect = path.getBoundingClientRect();
  const svg = transitionStyle.querySelector('svg');

  const scale = (resPathRect.height/svg.viewBox.baseVal.height) + 0.3;

  let tl = new TimelineLite({});

  const handRect = hand.getBoundingClientRect();
  const handMarginTop = parseFloat(getComputedStyle(hand).marginTop);

  const motionPath = MorphSVGPlugin.pathDataToBezier(path,
    {
      align: 'relative',
      matrix: [scale, 0, 0, scale, 0, 0]
    });

  tl
    .set(hand, {autoAlpha: 1})
    .fromTo(hand, 1,
      {
        x: window.innerWidth - handRect.left,
        y: window.innerHeight - handRect.top
      },
      {
        y: resPathRect.bottom - handRect.top + parseFloat(handMarginTop.toString()),
        x: resPathRect.right - handRect.left
      })
    // .to(hand, 4, {bezier:{values:motionPath, type:'cubic', ease: RoughEase.ease.config({ template:  Power1.easeNone, strength: 1, points: 23, taper: 'none', randomize:  true, clamp: false })}}, '+=0.5');
    .to(hand, 4, {bezier:{values:motionPath, type:'cubic', ease: Power0.easeNone}}, '+=0.5');

  return tl;
}
function _motionEraserAnimation(sectionOut, sectionIn, transitionStyle) {
  const path = transitionStyle.querySelector('path');
  let tl = new TimelineLite();

  path.setAttribute('stroke', getComputedStyle(document.documentElement).getPropertyValue(`--${sectionIn}-bg-color`).trim());

  tl.from(path, 4.5, {drawSVG: '0%', ease: Power2.easeOut});
  return tl;
}
export function eraserExitAnimation(sectionOut='frontpage', sectionIn) {
  const [tl, transitionStyle, charRect] = _bootstrapTransition(sectionOut, 'eraser');
  const char = document.getElementById(sectionOut).querySelector('.body-bg');

  tl
    .add(_handEraserAnimation(sectionOut, transitionStyle, charRect))
    .add(_motionEraserAnimation(sectionOut, sectionIn, transitionStyle), '-=3.8')
    .set(char, {autoAlpha: 0})
    .to(transitionStyle, 0.5, {autoAlpha: 0});

  return tl;
}

export function pushExitAnimation(sectionOut='frontpage') {
  const [tl, transitionStyle] = _bootstrapTransition(sectionOut, 'push');

  const pushBg = transitionStyle.querySelector('img');
  const pusher = transitionStyle.querySelector('#hand-push');
  const pusherRect = pusher.getBoundingClientRect();

  const rect = transitionStyle.getBoundingClientRect();

  const start = transitionStyle.querySelector('#hand-start');
  const tilt = transitionStyle.querySelector('#hand-tilt');
  const end =  transitionStyle.querySelector('#hand-end');

  const char = document.getElementById(sectionOut).querySelector('.body-bg');
  // const transitionStyle = document.querySelector(`#${sectionOut} .transition-e`);
  // const tl = new TimelineLite();

  tl
    .set(transitionStyle, {top: `-=${rect.top}px`, height: `+=${rect.top}px`})
    .set(pushBg, {autoAlpha: 1})
    .from(transitionStyle, 1, {
      x: `${pusherRect.width + (window.innerWidth - pusherRect.right)}px`,
      y: `-${rect.height + rect.top}px`,
      ease: Power2.easeOut
    })
    .to(start, 0.75, {morphSVG:{shape:tilt, shapeIndex:0}, ease:Power2.easeOut}, '-=0.2')
    .to(start, .2, {morphSVG:{shape:end, shapeIndex:0}, ease:Elastic.easeOut.config(2.5, 0.3)}, '+=0.5')
    .to(start, .5, {morphSVG:{shape:start, shapeIndex:0}, ease:Power2.easeOut})
    .to(char, 0.75, {rotation: '-=720_ccw', xPercent: -100, autoAlpha: 0, scale: 0.5, ease:Power2.easeOut}, '-=0.6')
    .to(transitionStyle, 0.5, {autoAlpha: 0});

  return tl;
}
